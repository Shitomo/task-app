\set dbuser `echo "$DB_USER"`
\set dbname `echo "$DB_NAME"`
\set dbpassword `echo "$DB_PASSWORD"`
CREATE ROLE :dbuser WITH LOGIN;
ALTER USER :dbuser WITH PASSWORD :'dbpassword';
CREATE DATABASE :dbname;
ALTER DATABASE :dbname OWNER TO :dbuser;