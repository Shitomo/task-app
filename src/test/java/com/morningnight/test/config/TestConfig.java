package com.morningnight.test.config;

import com.morningnight.domain.model.Task;
import com.morningnight.infrastructure.gateway.entity.AssigneeEntity;
import com.morningnight.infrastructure.gateway.mybatis.mapper.AssigneeMapper;
import com.morningnight.usecase.TaskService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

import static org.mockito.Mockito.*;

@Configuration
public class TestConfig {

    @Bean("taskService")
    public TaskService createMock() {
        TaskService taskService = mock(TaskService.class);
        initializeMock(taskService);
        return taskService;
    }

    public void initializeMock(TaskService taskService) {
        reset(taskService);
        doReturn(List.of(
                new Task(),
                new Task()
        )).when(taskService).getAllTasks();
    }
}

