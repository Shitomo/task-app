package com.morningnight.infrastructure.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.morningnight.domain.model.*;
import com.morningnight.infrastructure.gateway.mybatis.mapper.AssigneeMapper;
import com.morningnight.infrastructure.web.dto.AssigneeDto;
import com.morningnight.infrastructure.web.dto.CreateTaskAssigneeDto;
import com.morningnight.infrastructure.web.dto.CreateTaskDto;
import com.morningnight.infrastructure.web.dto.TaskDto;
import com.morningnight.usecase.TaskService;
import com.morningnight.usecase.UserService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import javax.sql.DataSource;

@ExtendWith(SpringExtension.class)
@ContextHierarchy({
        @ContextConfiguration("classpath:applicationContext.xml"),
        @ContextConfiguration("classpath:dispatcher-servlet.xml")
})
public class TaskControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private TaskService taskService;

    @Mock
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AssigneeMapper assigneeMapper;

    @Autowired
    private TaskController taskController;

    @BeforeEach
    public void setUp() throws Exception {
        // TODO : グローバルな設定にしたい．
        objectMapper.registerModule(new JavaTimeModule());
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(new TaskController(taskService, userService)).build();
    }

    @AfterAll
    public static void tearDown() throws Exception {

    }

    public void testMockito() {
        doReturn(10).when(userService).testMethod(9);
        doReturn(3, 4, 5, 6).when(userService).testMethod(anyInt());
        //　同じMatcherを二度書いた場合は後勝ち
        //doReturn(7, 8, 9, 10).when(userService).testMethod(anyInt());
        //　具体的な方が勝つは×
        //　同じメソッドに対するふるまいが複数定義された時，Mockitoはもっとも新しく定義されたものから調べます．
        //　例えば,
        // doReturn(10).when(userService).testMethod(9);
        // doReturn(3, 4, 5, 6).when(userService).testMethod(anyInt());
        // assertEquals(3, userService.testMethod(9));
        //　のように呼び出すと，Mockitoは
        // 1. 9が，anyInt()とマッチするか
        // 2. 9が,9とマッチするか(実際は1.でマッチして解決するのでこれは調べない)
        // という優先度で調べます．
        // より具体的な値にマッチするわけではないので注意が必要です．

        assertEquals(10, userService.testMethod(9));
    }

    /**
     * タスクリスト取得(正常系)
     */
    @Test
    @Sql({"/sql/reset_task_table.sql"})
    public void taskListTest() {
        try {
            List<Task> result = new ArrayList<>();
            //when(taskService.getAllTasks()).thenReturn(result);
            String response = mockMvc.perform(get("/tasks/"))
                    .andExpect(status().isOk())
                    .andExpect(content().json("[]"))
                    .andReturn().getResponse().getContentAsString();
            assertEquals("[]", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * タスクリスト取得(准正常系)
     *
     * 該当タスクが見つからなかった場合
     */
    @Test
    public void getTaskById() {
        try {
            List<Task> result = new ArrayList<>();
            //when(taskService.getAllTasks()).thenReturn(result);
            String response = mockMvc.perform(get("/tasks/" + 1))
                    .andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString();
            assertEquals("[]", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test void testBuildTaskDto() {
        // ユーザーサービスのモックの設定
        Map<String, User> testUserMap = Map.of(
                "user_id",
                User.createUserFromArgs("user_id", "b", UserStatus.Normal));
        // タスクサービスのモックの設定
        //when(userService.getUserMap(any())).thenReturn(testUserMap);
        Map<String, List<Assignee>> testAssigneesMap = Map.of(
                "task_id",
                List.of(
                        Assignee.create("user_id", AssignType.Main)
                )
        );
        //when(taskService.getAssigneesOfTasks(any())).thenReturn(testAssigneesMap);
        List<Task> input = List.of(
                Task.createTaskFromArgs(
                "task_id", "test", "test",null, null, false
                )
        );
        // 期待値の設定
        List<TaskDto> expected = List.of(
                TaskDto.of(
                        input.get(0),
                        List.of(
                ))
        );
        List<TaskDto> actual = taskController.buildTaskDtoList(input);
        assertEquals(expected, actual);
    }

    /**
     * 入力値が問題なければ，正常にタスクを作成できることのテスト
     */
    @Test
    public void create() {
        try {
            CreateTaskDto requestDto = new CreateTaskDto(){{
                setName("test");
                setDescription("test");
                setStartAt(LocalDateTime.now());
                setEndAt(LocalDateTime.now());
                setAssignees(List.of(
                    new CreateTaskAssigneeDto(){{
                        setId("a");
                        setType(AssignType.Main);
                    }})
                );
            }};
            List<Task> serviceResult = List.of(
                    requestDto.convertToModel()
            );
            //when(taskService.create(any(), any())).thenReturn(serviceResult);
            //TODO 日本語だと結果が文字化けして返ってくる現象の解決
            String response = mockMvc.perform(post("/tasks/")
                    .content(objectMapper.writeValueAsString(requestDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString();
            assertEquals(objectMapper.writeValueAsString(requestDto), response);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
}