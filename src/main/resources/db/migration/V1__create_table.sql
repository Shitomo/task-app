DROP TABLE IF EXISTS tasks;
DROP TABLE IF EXISTS taskapp_users;
DROP TABLE IF EXISTS task_assignees;
DROP INDEX IF EXISTS task_id_index;
DROP INDEX IF EXISTS taskapp_user_id_index;


CREATE TABLE tasks (
   id VARCHAR NOT NULL UNIQUE,
   name VARCHAR NOT NULL,
   description VARCHAR,
   start_at TIMESTAMP,
   end_at TIMESTAMP,
   is_completed BOOLEAN NOT NULL DEFAULT false
);

COMMENT ON TABLE tasks IS 'タスクのテーブル';
COMMENT ON COLUMN tasks.id IS 'タスクのID';
COMMENT ON COLUMN tasks.name IS 'タスクの名前';
COMMENT ON COLUMN tasks.description IS 'タスクの説明';
COMMENT ON COLUMN tasks.start_at IS 'タスクの開始日時';
COMMENT ON COLUMN tasks.end_at IS 'タスクの終了日時';
COMMENT ON COLUMN tasks.is_completed IS 'タスクの完了フラグ';

CREATE INDEX task_id_index ON tasks (
                                     id
    );

CREATE TYPE STATUS AS ENUM ('Normal', 'Suspended', 'None');

CREATE TABLE taskapp_users (
   id VARCHAR NOT NULL UNIQUE,
   name VARCHAR NOT NULL,
   status STATUS NOT NULL DEFAULT 'Normal'
);

COMMENT ON TABLE taskapp_users IS 'タスクアプリのユーザーテーブル';
COMMENT ON COLUMN taskapp_users.id IS 'タスクのユーザーのID';
COMMENT ON COLUMN taskapp_users.name IS 'タスクのユーザー名';

CREATE TYPE ASSIGN_TYPE AS ENUM ('Main', 'Sub');

CREATE TABLE task_assignees (
    id VARCHAR NOT NULL UNIQUE,
    task_id VARCHAR NOT NULL,
    taskapp_user_id VARCHAR NOT NULL,
    assign_type ASSIGN_TYPE,
    UNIQUE (task_id, taskapp_user_id)
);

CREATE INDEX taskapp_user_id_index ON task_assignees (
                                                      taskapp_user_id
    );

COMMENT ON TABLE taskapp_users IS 'タスクアプリのユーザーテーブル';
COMMENT ON COLUMN taskapp_users.id IS 'タスクのユーザーのID';
COMMENT ON COLUMN taskapp_users.name IS 'タスクのユーザー名';