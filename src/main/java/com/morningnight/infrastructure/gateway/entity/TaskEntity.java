package com.morningnight.infrastructure.gateway.entity;

import com.morningnight.domain.model.Task;
import java.time.LocalDateTime;

public class TaskEntity {
    private String id;
    private String name;
    private String description;
    private LocalDateTime startAt;
    private LocalDateTime endAt;
    private Boolean completed;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStartAt(LocalDateTime startAt) {
        this.startAt = startAt;
    }

    public void setEndAt(LocalDateTime endAt) {
        this.endAt = endAt;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public static TaskEntity create(Task task) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.id = task.getId();
        taskEntity.name = task.getName();
        taskEntity.description = task.getDescription();
        taskEntity.startAt = task.getStartAt();
        taskEntity.endAt = task.getEndAt();
        taskEntity.completed = task.getCompleted();
        return taskEntity;
    }
    
    public Task convertToModel() {
        return Task.createTaskFromArgs(
                this.getId(),
                this.getName(),
                this.getDescription(),
                this.getStartAt(),
                this.getEndAt(),
                this.getCompleted()
        );
    }
}
