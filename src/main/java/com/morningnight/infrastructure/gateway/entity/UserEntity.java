package com.morningnight.infrastructure.gateway.entity;

import com.morningnight.domain.model.AssignType;
import com.morningnight.domain.model.Task;
import com.morningnight.domain.model.User;
import com.morningnight.domain.model.UserStatus;

import java.util.List;

public class UserEntity {
    String id;
    String name;
    UserStatus status;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public UserStatus getStatus() {
        return status;
    }
}
