package com.morningnight.infrastructure.gateway.entity;

import com.morningnight.domain.model.AssignType;
import com.morningnight.domain.model.Assignee;

import java.util.UUID;

public class AssigneeEntity {
    String id;
    String taskId;
    String userId;
    AssignType assignType;

    public String getId() {
        return id;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getUserId() {
        return userId;
    }

    public AssignType getAssignType() {
        return assignType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setAssignType(AssignType assignType) {
        this.assignType = assignType;
    }

    public static AssigneeEntity create(String taskId, Assignee assignee) {
        AssigneeEntity assigneeEntity = new AssigneeEntity();
        assigneeEntity.id = UUID.randomUUID().toString();
        assigneeEntity.taskId = taskId;
        assigneeEntity.userId = assignee.getUserId();
        assigneeEntity.assignType = assignee.getAssignType();
        return assigneeEntity;
    }
}

