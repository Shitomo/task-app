package com.morningnight.infrastructure.gateway.repository;

import com.morningnight.domain.model.Task;
import com.morningnight.domain.model.User;
import com.morningnight.domain.repository.TaskRepository;
import com.morningnight.domain.repository.UserRepository;
import com.morningnight.infrastructure.gateway.entity.AssigneeEntity;
import com.morningnight.infrastructure.gateway.entity.TaskEntity;
import com.morningnight.infrastructure.gateway.entity.UserEntity;
import com.morningnight.infrastructure.gateway.mybatis.mapper.AssigneeMapper;
import com.morningnight.infrastructure.gateway.mybatis.mapper.TaskMapper;
import com.morningnight.infrastructure.gateway.mybatis.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository("userRepository")
public class UserRepositoryImpl implements UserRepository {

    private final AssigneeMapper assigneeMapper;
    private final UserMapper userMapper;

    public UserRepositoryImpl(
            @Autowired AssigneeMapper assigneeMapper,
            @Autowired UserMapper userMapper
    ) {
        this.assigneeMapper = assigneeMapper;
        this.userMapper = userMapper;
    }

    @Override
    public Map<String, User> findByTaskId(List<String> taskIds) {
        List<AssigneeEntity> assigneeEntities = assigneeMapper.findByTaskIds(taskIds);
        List<String> userIds = assigneeEntities.stream().map(
                AssigneeEntity::getUserId
        ).distinct().collect(Collectors.toList());
        List<UserEntity> userEntities = userMapper.findByIds(userIds);
        List<User> users =  userEntities.stream().map(
                userEntity -> User.createUserFromArgs(
                        userEntity.getId(),
                        userEntity.getName(),
                        userEntity.getStatus())
        ).collect(Collectors.toList());
        return users
                .stream()
                .collect(Collectors.toMap(User::getId, e -> e, (e1, e2) -> e1));
    }
}
