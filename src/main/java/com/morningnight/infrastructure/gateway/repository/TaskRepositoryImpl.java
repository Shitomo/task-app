package com.morningnight.infrastructure.gateway.repository;

import com.morningnight.domain.model.Assignee;
import com.morningnight.domain.model.Task;
import com.morningnight.domain.model.User;
import com.morningnight.domain.repository.TaskRepository;
import com.morningnight.infrastructure.gateway.entity.AssigneeEntity;
import com.morningnight.infrastructure.gateway.entity.TaskEntity;
import com.morningnight.infrastructure.gateway.entity.UserEntity;
import com.morningnight.infrastructure.gateway.mybatis.mapper.AssigneeMapper;
import com.morningnight.infrastructure.gateway.mybatis.mapper.TaskMapper;
import com.morningnight.infrastructure.gateway.mybatis.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository("taskRepository")
public class TaskRepositoryImpl implements TaskRepository {

    private final TaskMapper taskMapper;
    private final AssigneeMapper assigneeMapper;
    private final UserMapper userMapper;

    public TaskRepositoryImpl (
            @Autowired TaskMapper taskMapper,
            @Autowired AssigneeMapper assigneeMapper,
            @Autowired UserMapper userMapper
    ) {
        this.taskMapper = taskMapper;
        this.assigneeMapper = assigneeMapper;
        this.userMapper = userMapper;
    }


    @Override
    public List<Task> getAllTasks() {
        List<TaskEntity> taskEntities = taskMapper.findAll();
        return taskEntities.stream().map(
                TaskEntity::convertToModel
        ).collect(Collectors.toList());
    }

    @Override
    public List<Task> getTaskInfo(String id) {
        List<TaskEntity> taskEntities = taskMapper.findById(id);
        return taskEntities.stream().map(
                TaskEntity::convertToModel
        ).collect(Collectors.toList());
    }

    @Override
    public Map<String,List<Assignee>> getAssigneeOfTasks(List<String> ids) {
        List<AssigneeEntity> assigneeEntities = assigneeMapper.findByTaskIds(ids);
        Map<String, List<Assignee>> assigneeMap = new HashMap<>();
        for (String taskId : ids) {
            List<Assignee> assigneesOfTask = assigneeEntities.stream().filter(
                    e -> Objects.equals(e.getTaskId(), taskId)
            ).map(
                    assignee -> Assignee.create(assignee.getUserId(), assignee.getAssignType())
            ).collect(Collectors.toList());
            assigneeMap.put(taskId, assigneesOfTask);
        }
        return assigneeMap;
    }

    @Override
    public void save(Task task, List<Assignee> assignees) {
        TaskEntity taskEntity = TaskEntity.create(task);
        taskMapper.insert(taskEntity);
        List<AssigneeEntity> assigneeEntities = assignees.stream().map(
                assignee -> AssigneeEntity.create(task.getId(), assignee)
        ).collect(Collectors.toList());
        List<String> userIds = assigneeEntities.stream().map(
                AssigneeEntity::getUserId
        ).collect(Collectors.toList());
        List<UserEntity> userEntities = userMapper.findByIds(userIds);
        // 存在していないユーザー，状態が不正なユーザーをはじく処理
        assigneeMapper.insertAssignees(assigneeEntities);
    }
}
