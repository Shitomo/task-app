package com.morningnight.infrastructure.gateway.mybatis.mapper;

import com.morningnight.infrastructure.gateway.entity.AssigneeEntity;
import com.morningnight.infrastructure.gateway.entity.TaskEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AssigneeMapper {
    List<AssigneeEntity> findByTaskIds(@Param("taskIds") List<String> taskIds);

    List<AssigneeEntity> findByTaskId(@Param("taskId") String taskId);

    void insertAssignees(@Param("assignees") List<AssigneeEntity> assignees);
}
