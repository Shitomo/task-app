package com.morningnight.infrastructure.gateway.mybatis;

import com.morningnight.domain.model.AssignType;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AssignTypeTypeHandler extends BaseTypeHandler<AssignType> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, AssignType parameter, JdbcType jdbcType)
            throws SQLException {
        ps.setString(i, parameter.toString());
    }

    @Override
    public AssignType getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return AssignType.fromString(rs.getString(columnName));
    }

    @Override
    public AssignType getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return AssignType.fromString(rs.getString(columnIndex));
    }

    @Override
    public AssignType getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return AssignType.fromString(cs.getString(columnIndex));
    }
}
