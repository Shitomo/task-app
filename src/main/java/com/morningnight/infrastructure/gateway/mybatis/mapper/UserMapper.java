package com.morningnight.infrastructure.gateway.mybatis.mapper;

import com.morningnight.infrastructure.gateway.entity.UserEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    List<UserEntity> findByIds(@Param("ids") List<String> ids);

    List<UserEntity> findById(@Param("id") List<String> id);
}
