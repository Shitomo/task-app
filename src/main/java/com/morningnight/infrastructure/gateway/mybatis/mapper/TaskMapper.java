package com.morningnight.infrastructure.gateway.mybatis.mapper;

import com.morningnight.domain.model.Task;
import com.morningnight.infrastructure.gateway.entity.TaskEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TaskMapper {
    List<TaskEntity> findAll();

    List<TaskEntity> findById(String id);

    void insert(@Param("task") TaskEntity task);
}
