package com.morningnight.infrastructure.web.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningnight.domain.model.Task;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;

public class CreateTaskDto {
    @NotBlank
    @Max(value = 1000, message = "")
    @JsonProperty("name")
    private String name;
    @Max(1000)
    @JsonProperty("description")
    private String description;
    @JsonProperty("start_at")
    @JsonFormat(pattern="yyyy-MM-dd-HHmmss")
    private LocalDateTime startAt;
    @JsonProperty("end_at")
    @JsonFormat(pattern="yyyy-MM-dd-HHmmss")
    private LocalDateTime endAt;
    @Valid
    @JsonProperty("assignees")
    private List<CreateTaskAssigneeDto> assignees;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public List<CreateTaskAssigneeDto> getAssignees() {
        return assignees;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStartAt(LocalDateTime startAt) {
        this.startAt = startAt;
    }

    public void setEndAt(LocalDateTime endAt) {
        this.endAt = endAt;
    }

    public void setAssignees(List<CreateTaskAssigneeDto> assignees) {
        this.assignees = assignees;
    }

    public Task convertToModel() {
        return Task.createNewTask(this.name, this.description, this.startAt, this.endAt);
    }
}
