package com.morningnight.infrastructure.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningnight.domain.model.AssignType;
import com.morningnight.domain.model.Assignee;
import com.morningnight.domain.model.User;

import java.util.Objects;

public class AssigneeDto {
    @JsonProperty("user_id")
    String id;
    @JsonProperty("username")
    String name;
    @JsonProperty("assign_type")
    AssignType type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AssignType getType() {
        return type;
    }

    public void setType(AssignType type) {
        this.type = type;
    }

    public static AssigneeDto of(Assignee assignee, User userInfo) {
        AssigneeDto assigneeDto = new AssigneeDto();
        assigneeDto.id = userInfo.getId();
        assigneeDto.name = userInfo.getName();
        assigneeDto.type = assignee.getAssignType();
        return assigneeDto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssigneeDto)) return false;
        AssigneeDto that = (AssigneeDto) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName()) && getType() == that.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getType());
    }

    @Override
    public String toString() {
        return "AssigneeDto{" +
                "id='" + id + '\'' + '\n' +
                ", name='" + name + '\'' + '\n' +
                ", type=" + type + '\n' +
                '}';
    }
}
