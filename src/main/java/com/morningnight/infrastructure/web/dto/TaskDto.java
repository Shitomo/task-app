package com.morningnight.infrastructure.web.dto;

import com.morningnight.domain.model.Task;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class TaskDto implements Serializable {
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("start_at")
    @JsonFormat(pattern="yyyy-MM-dd-HHmmss")
    private LocalDateTime startAt;
    @JsonProperty("end_at")
    @JsonFormat(pattern="yyyy-MM-dd-HHmmss")
    private LocalDateTime endAt;
    @JsonProperty("assignees")
    private List<AssigneeDto> assignees;
    @JsonProperty("is_completed")
    private Boolean isCompleted;
    @JsonProperty("created_at")
    private LocalDateTime createdAt;

    public static TaskDto of(Task task, List<AssigneeDto> assignees) {
        TaskDto taskDto = new TaskDto();
        taskDto.id = task.getId();
        taskDto.name = task.getName();
        taskDto.description = task.getDescription();
        taskDto.startAt = task.getStartAt();
        taskDto.endAt = task.getEndAt();
        taskDto.assignees = assignees;
        taskDto.isCompleted = task.getCompleted();
        taskDto.createdAt = task.getCreatedAt();
        return taskDto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskDto)) return false;
        TaskDto taskDto = (TaskDto) o;
        return Objects.equals(id, taskDto.id) && Objects.equals(name, taskDto.name) && Objects.equals(description, taskDto.description) && Objects.equals(startAt, taskDto.startAt) && Objects.equals(endAt, taskDto.endAt) && Objects.equals(assignees, taskDto.assignees) && Objects.equals(isCompleted, taskDto.isCompleted) && Objects.equals(createdAt, taskDto.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, startAt, endAt, assignees, isCompleted, createdAt);
    }

    @Override
    public String toString() {
        return "TaskDto{" +
                "id='" + id + '\'' + '\n' +
                ", name='" + name + '\'' + '\n' +
                ", description='" + description + '\'' + '\n' +
                ", startAt=" + startAt + '\n' +
                ", endAt=" + endAt + '\n' +
                ", assignees=" + assignees + '\n' +
                ", isCompleted=" + isCompleted + '\n' +
                ", createdAt=" + createdAt + '\n' +
                '}';
    }
}
