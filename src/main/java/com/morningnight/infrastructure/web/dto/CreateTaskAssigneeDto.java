package com.morningnight.infrastructure.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.morningnight.domain.model.AssignType;
import com.morningnight.domain.model.Assignee;
import javax.validation.constraints.NotNull;

public class CreateTaskAssigneeDto {
    @NotNull
    @JsonProperty("user_id")
    String id;
    @NotNull
    @JsonProperty("assign_type")
    AssignType type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AssignType getType() {
        return type;
    }

    public void setType(AssignType type) {
        this.type = type;
    }

    public Assignee convertToModel() {
        return Assignee.create(this.id, this.type);
    }
}
