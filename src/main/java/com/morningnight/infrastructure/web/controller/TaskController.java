package com.morningnight.infrastructure.web.controller;

import com.morningnight.domain.model.Assignee;
import com.morningnight.domain.model.Task;
import com.morningnight.domain.model.User;
import com.morningnight.infrastructure.web.dto.AssigneeDto;
import com.morningnight.infrastructure.web.dto.CreateTaskAssigneeDto;
import com.morningnight.infrastructure.web.dto.CreateTaskDto;
import com.morningnight.infrastructure.web.dto.TaskDto;
import com.morningnight.usecase.TaskService;
import com.morningnight.usecase.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("tasks")
public class TaskController {

    private final TaskService taskService;
    private final UserService userService;

    public TaskController(
            @Autowired TaskService taskService,
            @Autowired UserService userService
    ) {
        this.taskService = taskService;
        this.userService = userService;
    }

    @GetMapping("/")
    public List<TaskDto> list() {
        List<Task> tasks = taskService.getAllTasks();
        return buildTaskDtoList(tasks);
    }

    @GetMapping("/{taskId}")
    public List<TaskDto> getTaskInfo(@PathVariable String taskId) {
        List<Task> tasks = taskService.getTaskInfo(taskId);
        return buildTaskDtoList(tasks);
    }

    @PostMapping("/")
    public CreateTaskDto post(@Validated @RequestBody CreateTaskDto createTaskDto) {
        Task task = createTaskDto.convertToModel();
        List<Assignee> assignees = createTaskDto.getAssignees().stream().map(
                CreateTaskAssigneeDto::convertToModel
        ).collect(Collectors.toList());
        this.taskService.create(task, assignees);
        return createTaskDto;
    }

    public List<TaskDto> buildTaskDtoList(List<Task> tasks) {
        List<String> taskIds = tasks.stream().map(
                Task::getId
        ).collect(Collectors.toList());
        Map<String, User> userMap = userService.getUserMap(taskIds);
        Map<String, List<Assignee>> taskIdToAssigneesMap = taskService.getAssigneesOfTasks(taskIds);
        List<TaskDto> taskDtos = new ArrayList<>();
        for (Task task : tasks) {
            List<Assignee> assignees = taskIdToAssigneesMap.get(task.getId());
            List<AssigneeDto> assigneeDtos = assignees.stream().map(
                    assignee -> {
                        User user = userMap.get(assignee.getUserId());
                        return AssigneeDto.of(assignee, user);
                    }
            ).collect(Collectors.toList());
            taskDtos.add(TaskDto.of(task, assigneeDtos));
        }
        return taskDtos;
    }
}
