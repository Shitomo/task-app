package com.morningnight.usecase;

import com.morningnight.domain.model.Task;
import com.morningnight.domain.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * タスクに関するユースケースをまとめるインターフェース
 */
@Service
public interface UserService {
    /**
     * タスクに紐づいているユーザーの，ユーザーIDとユーザー詳細情報のマップを取得
     */
    Map<String, User> getUserMap(List<String> taskIds);

    int testMethod(int value);
}