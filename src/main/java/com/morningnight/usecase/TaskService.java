package com.morningnight.usecase;

import com.morningnight.domain.model.Assignee;
import com.morningnight.domain.model.Task;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * タスクに関するユースケースをまとめるインターフェース
 */
@Service
public interface TaskService {
    /**
     * タスクの全取得
     *
     * @return List<Task> 全てのタスクのリスト
     */
    List<Task> getAllTasks();

    /**
     * タスクに紐づく担当者一覧
     *
     * @return List<Assignee> 担当者のリスト
     */
    Map<String, List<Assignee>> getAssigneesOfTasks(List<String> taskIds);

    /**
     * 特定のタスクの詳細情報取得
     *
     * @return Task タスク
     */
    List<Task> getTaskInfo(String id);

    /**
     * タスク作成
     *
     * @return Task タスク
     */
    List<Task> create(Task task, List<Assignee> assignees);
}
