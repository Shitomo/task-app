package com.morningnight.usecase.impl;

import com.morningnight.domain.model.Assignee;
import com.morningnight.domain.model.Task;
import com.morningnight.domain.repository.TaskRepository;
import com.morningnight.usecase.TaskService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("taskService")
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    public TaskServiceImpl(
            @Autowired TaskRepository taskRepository
    ) {
        this.taskRepository = taskRepository;
    }

    /**
     * @see(com.morningnight.usecase.TaskService#getAllTasks())
     */
    @Override
    public List<Task> getAllTasks() {
        return taskRepository.getAllTasks();
    }

    @Override
    public Map<String, List<Assignee>> getAssigneesOfTasks(List<String> taskIds) {
        return taskRepository.getAssigneeOfTasks(taskIds);
    }

    /**
     * @see(com.morningnight.usecase.TaskService#getTaskInfo())
     */
    @Override
    public List<Task> getTaskInfo(String id) {
        return taskRepository.getTaskInfo(id);
    }

    /**
     * @see(com.morningnight.usecase.TaskService#create())
     */
    @Override
    public List<Task> create(Task task, List<Assignee> assignees) {
        taskRepository.save(task, assignees);
        return new ArrayList<>();
    }


}
