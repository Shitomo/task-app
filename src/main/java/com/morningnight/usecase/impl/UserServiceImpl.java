package com.morningnight.usecase.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.morningnight.domain.model.User;
import com.morningnight.domain.repository.UserRepository;
import com.morningnight.usecase.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("userService")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(
            @Autowired UserRepository userRepository
    ) {
        this.userRepository = userRepository;
    }

    public Map<String, User> getUserMap(List<String> taskIds) {
        return this.userRepository.findByTaskId(taskIds);
    }

    public int testMethod(int a) {
        return 0;
    }
}
