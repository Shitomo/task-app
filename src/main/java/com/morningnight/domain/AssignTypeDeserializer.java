package com.morningnight.domain;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.morningnight.domain.model.AssignType;

import java.io.IOException;

public class AssignTypeDeserializer extends JsonDeserializer<AssignType> {

    @Override
    public AssignType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        final String jsonValue = jsonParser.getText();
        return AssignType.fromString(jsonValue);
    }
}
