package com.morningnight.domain.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * タスクのドメインモデル
 */
public class Task {
    // ID
    private String id;
    // タスク名
    private String name;
    // タスクの説明
    private String description;
    // タスクの開始日
    private LocalDateTime startAt;
    // タスクの終了日
    private LocalDateTime endAt;
    // 完了フラグ
    private Boolean isCompleted;
    // 作成日時
    private LocalDateTime createdAt;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getStartAt() {
        return startAt;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public Boolean getCompleted() {
        return isCompleted;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public static Task createTaskFromArgs(String id, String name, String description, LocalDateTime startAt, LocalDateTime endAt, Boolean isCompleted) {
        Task task = new Task();
        task.id = id;
        task.name = name;
        task.description = description;
        task.startAt = startAt;
        task.endAt = endAt;
        task.isCompleted = isCompleted;
        return task;
    }

    public static Task createNewTask(String name, String description, LocalDateTime startAt, LocalDateTime endAt) {
        Task task = new Task();
        task.id = UUID.randomUUID().toString();
        task.name = name;
        task.description = description;
        task.startAt = startAt;
        task.endAt = endAt;
        task.isCompleted = false;
        task.createdAt = LocalDateTime.now();
        return task;
    }
}
