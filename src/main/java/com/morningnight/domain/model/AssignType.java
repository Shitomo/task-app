package com.morningnight.domain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.morningnight.domain.AssignTypeDeserializer;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toMap;

/**
 * 担当種別
 */
@JsonDeserialize(using = AssignTypeDeserializer.class)
public enum AssignType {
    // メイン担当
    Main("Main"),
    // サブ担当
    Sub("Sub"),
    // 種別なし
    None("")
    ;

    private static Map<String, AssignType> FORMAT_MAP = Stream
            .of(AssignType.values())
            .collect(toMap(s -> s.formatted, Function.<AssignType>identity()));

    private final String formatted;

    AssignType(String formatted) {
        this.formatted = formatted;
    }

    @JsonValue
    public String toString() {
        return formatted;
    }

    @JsonCreator
    public static AssignType fromString(String string) {
        AssignType assignType = FORMAT_MAP.get(string);
        if (assignType == null) {
            throw new IllegalArgumentException(string + " has no corresponding value");
        }
        return assignType;
    }
}
