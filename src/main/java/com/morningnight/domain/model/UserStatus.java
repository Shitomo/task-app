package com.morningnight.domain.model;

/**
 * ユーザーの登録状態を表すドメインモデル
 */
public enum UserStatus {
    // 通常
    Normal("Normal"),
    // 凍結中
    Suspended("Suspended"),
    ;

    private final String name;

    private UserStatus(final String userStatusName) {
        this.name = userStatusName;
    }

    public String getString() {
        return this.name;
    }

    public static UserStatus getUserStatus(final String name) {
        UserStatus[] statuses = UserStatus.values();
        for (UserStatus status : statuses) {
            if (status.getString().equals(name)) {
                return status;
            }
        }
        return Normal;
    }
}
