package com.morningnight.domain.model;

import java.util.UUID;

/**
 * ユーザーのドメインモデル
 */
public class User {
    String id;
    String name;
    UserStatus status;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public UserStatus getStatus() {
        return status;
    }

    public static User createUserFromArgs(String id,  String name, UserStatus status) {
        User user = new User();
        user.id = id;
        user.name = name;
        user.status = status;
        return user;
    }

}
