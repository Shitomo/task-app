package com.morningnight.domain.model;

/**
 * タスク担当者のドメインモデル
 */
public class Assignee {
    String userId;
    AssignType assignType;

    public String getUserId() {
        return userId;
    }

    public AssignType getAssignType() {
        return assignType;
    }

    public static Assignee create(String userId, AssignType type) {
        Assignee assignee = new Assignee();
        assignee.userId = userId;
        assignee.assignType = type;
        return assignee;
    }
}
