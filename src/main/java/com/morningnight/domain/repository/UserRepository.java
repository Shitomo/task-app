package com.morningnight.domain.repository;

import com.morningnight.domain.model.User;

import java.util.List;
import java.util.Map;

public interface UserRepository {
    Map<String, User> findByTaskId(List<String> taskId);
}
