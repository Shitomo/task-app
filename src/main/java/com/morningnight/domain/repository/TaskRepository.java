package com.morningnight.domain.repository;

import com.morningnight.domain.model.Assignee;
import com.morningnight.domain.model.Task;

import java.util.List;
import java.util.Map;

public interface TaskRepository {
    List<Task> getAllTasks();

    List<Task> getTaskInfo(String id);

    Map<String, List<Assignee>> getAssigneeOfTasks(List<String> ids);

    void save(Task task, List<Assignee> assignees);
}
